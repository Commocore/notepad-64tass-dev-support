﻿
Development environment support for Notepad++ editor and 64tass cross-assembler
============================================================

Prerequisites: Installation of plugins and their configuration may vary depending on their versions and installation of _Notepad++_.
For the best result, use Plugins Admin available from the Plugins tab in _Notepad++_.


1. Installation of _64tass_ syntax highlighting.

	Go to Language > Define your language... Then select Import... and select [64tass-highlighting.xml](./64tass-highlighting.xml) file available in this repository, or use the
	highlighting provided within the 64tass package.
	
	From now, on each open file, you can enjoy the _64tass_ syntax highlighting.


2. Setting up Function List for _64tass_.

	Function List allows to list all functions in currently opened file.

	For _Notepad++_ v7.9.1 and later versions:
	
	2.1. Go to installation of Notepad++ (e.g. `F:\Program Files\Notepad++`) and open `overrideMap.xml` file from `functionList` folder.
	
	2.2. Add the association for the custom user language:
	
	
		<association id="64tass.xml" userDefinedLangName="64tass" />
		
		
	2.3. Create file inside the `functionList` folder titled `64tass.xml` and add the content from [function-list/npp-v7.9.1-and-later/64tass.xml](./function-list/npp-v7.9.1-and-later/64tass.xml) file available in this repository.
	
	Continue with point 2.4 below.
	
	
	For _Notepad++_ v7.9 and earlier versions:
	
	
	2.1. Open function list config file to edit: `C:\Users\<Username>\AppData\Roaming\Notepad++\functionList.xml`.
	
	2.2. Add parser to `<parsers>` section from [function-list/npp-v7.9-and-earlier/functionList.xml](./function-list/npp-v7.9-and-earlier/functionList.xml) file available in this repository.
	
	2.3. Add association to `<associationMap>` section from [function-list/npp-v7.9-and-earlier/functionList.xml](./function-list/npp-v7.9-and-earlier/functionList.xml) file available in this repository.
	
	2.4. You have to re-open _Notepad++_ to see the changes. Function List window can be enabled from top menu: View > Function List.

	For the given examples:

	- rts
	- rti
	- ZP = $54
	- dinosaurFunction
	- start    .word 5
	- ; just a comment
	- .word $5008

	these functions are matched by regex pattern and displayed on Function List:

	- ZP
	- dinosaurFunction
	- start


3. Enable jumping to procedures across the whole project and returning back.

	Two solutions have been tested, so feel free to read about the details of each before installing. You can
	also use both solutions at the same time, however you have to re-assign hotkeys as some of they may conflict.

	3.1. SourceCookifier.

	Note that I superseded SourceCookifier with NppGTags plugin which keeps DB in files while SourceCookifier needs it to be added
	manually on each restart of Notepad++ which can be really annoying, especially that it takes time to re-index definitions in bigger projects.
	
	3.1.1. Install SourceCookifier plugin and toggle its window by selecting option from Plugins > SourceCookifier > Toogle SourceCookifier.
	
	3.1.2. Drag project folder into the window. SourceCookifier will scan all files. Then you will be asked to select all files which SourceCookifier should track.
	Select files with the extension you're using for assembly code (I'm using *.asm extension).
	
	Be sure that `labels` are shown by enabling them with the right click mouse button on the SourceCookifier list: Show > labels. Otherwise it won't work.
	
	From now on, you're able to jump to any label, i.e. procedure or a variable definition by:
	
	- setting a cursor under the procedure name and pressing `Shift` + `Ctrl` + `Enter`
	- pressing `Ctrl` key and clicking with the left mouse button
	
	You can navigate back and forth with `Alt` + `Left` cursor key or `Alt` + `Right` cursor key.
	
	3.2. TagLEET.
	
	3.2.1. Install TagLEET plugin from [https://github.com/vinsworldcom/nppTagLEET/releases](https://github.com/vinsworldcom/nppTagLEET/releases) into plugin
	folder so `TagLEET` folder contains `TagLEET.dll` file and yet another `TagLEET` folder with `ctags.exe` file.
	
	3.2.2. By default, `Alt` + `Space` is used as a hotkey for lookup.
	I used this key combination for another feature (to search for files in SolutionHub), so as it conflicted with this one set as default by `TagLEET`,
	it's good to find another shortcut.
	
	To change it, navigate to Settings > Shortcut Mapper... Then, switch to the Plugin commands tab and filter the list by "TagLEET".
	Unfortunately, using a left click mouse (compared to SourceCookifier) is not possible in case of TagLEET. You can use only keys.
	
	3.2.3. Navigate to Plugins > TagLEET > Settings and check Update tags file on save. Otherwise, new changes won't be indexed.
	
	3.2.4. For the first time of use, when you'll try to do a lookup with a hotkey, you will be prompted to provide the source folder.
	After selecting a folder, tags file with indexed labels will be created automatically and saved in the current project as long as no global tags file
	is set in the settings (Plugins > TagLEET > Settings under Global Tags File option).
	
	From now on, set a cursor under the procedure name and press a hotkey to lookup. In the popup window you'll see
	a list of procedures and a preview. By pressing the `Enter` key, you will jump to the procedure.
	
	You can navigate back and forth with `Alt` + `Left` cursor key or `Alt` + `Right` cursor key.
	
	Note: I think that navigating with that is slightly inconvenient, at least for me. I use NppGTags plugin for that (see below).
	
	3.3. NppGTags.
	
	3.3.1. From the top menu go to Plugins > NppGtags > Settings.
	
	3.3.2. Set DB to use `ctags` and create DB. This unfortunately creates files `GPATH`, `GRTAGS`, `GTAGS` and `NppGTags.cfg` in the main root of the project,
	so ignore these files with global or local `.gitignore` file. Unfortunately, the other downside of using `ctags` is that
	it doesn't support reference search, but only jumping to definitions, but at least this feature works flawless.
	
	3.3.3. Too lookup definitions with a shortcut, you could use for example F1 key.
	Shortcuts can be configured by going from the top menu to Run > Modify Shortcut/Delete Command in "Plugin commands" tab.
	You could re-use `F1` key by disabling it first in shortcuts as by default it's used to show About.
	
	3.3.4. Then it's also convenient to configure mapping to navigate back from the definition jump (e.g. with `ALT` + `[`) and forth (e.g. with `ALT` + `]`).
	
	- if you don't want to see the results on every DB update, switch it off in Plugins > NppGTags console > Toggle Results Window Focus.
	- cool thing about this feature is that you can also press `F1` key without selecting any definition to type in its name.
	

4. Compiling and running.

	4.1. Download [NppExec plugin](https://sourceforge.net/projects/npp-plugins/files/NppExec/).
	
	4.2. Unarchive and install the plugin by moving the `NppExec` folder with `NppExec.dll` file to `C:\Program Files\Notepad++\plugins` (Notepad++ location might vary depending on your installation).
	Also, copy other files if you're going to use breakpoints functionality (see: point 6).
	
	4.3. Now, by pressing `F6`, _NppExec_ window will pop out. In this window you can define commands and save under a specific name, e.g. "Compile & run my lovely project".
	
	For more details, check [npp-exec-example.txt](./npp-exec-example.txt) which contains not only compilation but also running _Vice_ emulator afterwards.
	
	So, if you use _Vice_ emulator and want to run it automatically after compiling, you can use [Kick Vice](https://bitbucket.org/Commocore/kick-vice) script for running your projects (you will need `kick-vice.bat` and `kick-vice.vbs`.


5. Quick search & go to file.

	5.1. Feel free to follow the instructions below or from [http://npp.incrediblejunior.com/](http://npp.incrediblejunior.com/). Anyway, you will probably need to download SolutionHub, SolutionHubUI and Open File In Solution plugins from that link in case you have problems installing these plugins. Though, Plugins Admin from Plugins tab should do the job.
	
	5.2. Create a new solution by navigating to Plugins > SolutionHubUI > SolutionHubUIShow. Type the name of the solution first, followed by selecting the path below with Recursive and Monitored options checked. Most likely you want to create a solution for a single project, so just use the name of the project and choose the folder where project resides. Then Save solution and keep the window still open.
	
	5.3. Now, let's connect the solution with Open File In Solution plugin. In the right column "Connections" next to the freshly created solution, type "ofis" (that stands for Open File In Solution). Save connections and exit.

	5.3. Navigate to Settings > Shortcut mapper... and select the Plugin commands tab. Over there navigate to `OFIS - Show` definition and modify it by providing the shortcut. I use `Alt` + `Space` but feel free to go with your heart or habits.

	Use the shortcut to open the Open File In Solution pop up window. From there you will be able to type the part of the filename you want to open in the selected previously path. You can use up & down cursor keys to highlight the file you want to open. Then, press the `Enter` to open it.

	Note: if your files contain hyphens, do not use them as this character is reserved for excluding in search. So yes, use it if you want to exclude something from the search results.

6. Simple breakpoints on labels.

	Notepad++ doesn't have functionality to debug, but thanks to bookmarks functionality, with a little effort it's possible to create a list of breakpoints and send them over to Vice64 emulator on compiling or execution.
	By setting bookmarks next to a label, these can be exported to Vice64 emulator. This way, code execution will be interrupted followed by opening the assembly monitor
	in case the given label will be executed.
	
	Note: The script presented for this solution works only for setting breakpoints in the currently opened file. It also doesn't scan through the whole workspace.

	6.1. In the NppExec plugin, 4 files need to be present as they are needed to send messages to Scintilla library.
	You'll find these files in `NppExec\Release\NppExec` folder. Put them into `plugins\NppExec\NppExec` folder of your local Notepad++ installation (e.g. `C:\Program Files\Notepad++\plugins`).
	
	- `BaseDef.h`
	- `menuCmdlD.h`
	- `Notepad_plus_msgs.h`
	- `Scintilla.h`
	
	6.2. Next, copy `npp-exec-breakpoints.txt` file from this repository and add as a script to your NppExec script (I recommend including it in your NppExec script with `NPP_EXEC`).
	It will scan the currently open file for bookmarks and will save them to a specified file under `BREAKPOINTS_FILE` local variable.
	The output file will contain the list of simple (so without any conditions) executable breakpoints, e.g.:
	
	```
	break exec .myLabel
	break exec .myOtherLabel
	```
	
	6.3. To pass breakpoint labels to Vice64 emulator, you can provide monitor commands file that will execute loading labels from the created file.
	You can provide this file the following way:
	
	```
	x64.exe" -moncommands monitor-commands.txt
	```

	And inside the `monitor-commands.txt` file where you can add multiple commands, let's add the one for the breakpoint labels:
	```
	load_labels "build\labels\breakpoints.txt"
	```
	
	Don't worry if the breakpoint file does not exist. It won't break the execution of other commands.
	
	Hint: to return from the breakpoint in the Vice64 assembly monitor to code execution, just type `g`.

	Note: script is able to ignore anything that follows the label in the same line, e.g. whitespaces, comments or scope symboles (e.g. `.block`, `.proc` etc.).
	However, be aware that labels defined inside scopes are not exported by 64tass cross-compiler so they will be ignored. One way of dealing with it is to do a jump to a label set as a breakpoint outside that block.
	